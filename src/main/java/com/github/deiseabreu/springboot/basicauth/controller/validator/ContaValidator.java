package com.github.deiseabreu.springboot.basicauth.controller.validator;

import com.github.deiseabreu.springboot.basicauth.model.Conta;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class ContaValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Conta.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "validation.message.field.required");
    }

}
