package com.github.deiseabreu.springboot.basicauth.configuration.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

@Configuration
@EnableWebSecurity
public class CustomWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    private static final String CONTAS_URL = "/v1/contas";
    private static final String CONTA_URL = "/v1/conta**";

    private static final String ADMIN_USER = "admin";
    private static final String ADMIN_ROLE = "ADMIN";
    private static final String BASIC_ROLE = "BASIC";
    private static final String BASIC_USER = "user";

    private final AccessDenied accessDenied = new AccessDenied();

    private final CustomBasicAuthenticationEntryPoint authenticationEntryPoint = new CustomBasicAuthenticationEntryPoint();

    private final CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler = new CustomAuthenticationSuccessHandler();

    private final SimpleUrlAuthenticationFailureHandler simpleUrlAuthenticationFailureHandler = new SimpleUrlAuthenticationFailureHandler();

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser(ADMIN_USER).password(encoder().encode(ADMIN_USER)).roles(ADMIN_ROLE).and()
                .withUser(BASIC_USER).password(encoder().encode(BASIC_USER)).roles(BASIC_ROLE);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        System.out.println("1");
        http.csrf().disable()
                .authorizeRequests()
                .and().exceptionHandling()
                .accessDeniedHandler(accessDenied)
                .authenticationEntryPoint(authenticationEntryPoint)
                .and().authorizeRequests()
                .antMatchers("/").hasAnyRole(ADMIN_ROLE)
                .antMatchers("/v1/conta/**").hasAnyRole(BASIC_ROLE, ADMIN_ROLE)
                .antMatchers("/login**").permitAll()
                .antMatchers("/logout**").hasAnyRole(BASIC_ROLE, ADMIN_ROLE)
                .antMatchers(HttpMethod.GET, CONTAS_URL).hasAnyRole(BASIC_ROLE, ADMIN_ROLE)
                .antMatchers(HttpMethod.GET, CONTA_URL).hasAnyRole(BASIC_ROLE, ADMIN_ROLE)
                .antMatchers(HttpMethod.PUT, CONTA_URL).hasRole(ADMIN_ROLE)
                .antMatchers(HttpMethod.DELETE, CONTA_URL).hasRole(ADMIN_ROLE)
                .and().formLogin()
                .successHandler(customAuthenticationSuccessHandler)
                .failureHandler(simpleUrlAuthenticationFailureHandler)
                .and().httpBasic()
                .and().logout();
    }

    @Bean
    public PasswordEncoder  encoder() {
        return new BCryptPasswordEncoder();
    }

}
