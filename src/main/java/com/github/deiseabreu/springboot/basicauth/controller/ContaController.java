package com.github.deiseabreu.springboot.basicauth.controller;

import javax.validation.Valid;

import com.github.deiseabreu.springboot.basicauth.controller.validator.ContaValidator;
import com.github.deiseabreu.springboot.basicauth.model.Conta;
import com.github.deiseabreu.springboot.basicauth.service.ContaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
@Validated
public class ContaController {

    public static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    ContaService contaService;

    @RequestMapping(path = "/v1/contas", method = RequestMethod.GET)
    @ApiOperation(
            value = "Listar todas as contas",
            notes = "Retorna uma quantidade N de contas de acordo com o parâmetro informado.",
            response = Page.class)
    public Page<Conta> getAll(
            @ApiParam("Quantidade total a ser retornada") @RequestParam(required = false) Integer size,
            @ApiParam("Offset de paginação") @RequestParam(required = false) Integer page) {

        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        if (page == null) {
            page = 0;
        }

        Pageable pageable = new PageRequest(page, size);
        Page<Conta> contas = contaService.findAll(pageable);

        return contas;
    }

    @RequestMapping(path = "/v1/conta/{id}", method = RequestMethod.GET)
    @ApiOperation(
            value = "Recuperar conta pelo ID",
            notes = "Retorna uma conta pelo ID informado.",
            response = Conta.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "Conta não localizada") })
    public ResponseEntity<Conta> get(@ApiParam("Identificador da conta") @PathVariable("id") Long id) {

        Conta conta = contaService.findOne(id);
        return (conta == null ? ResponseEntity.status(HttpStatus.NOT_FOUND) : ResponseEntity.ok()).body(conta);
    }

    @RequestMapping(path = "/v1/conta", method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @ApiOperation(
            value = "Cria ou atualiza uma conta já existente",
            notes = "Cria ou atualiza uma conta. Retorna created/updated com o identificador da conta.",
            response = Conta.class)
    public ResponseEntity<Conta> add(
            @Valid @RequestBody Conta conta) {

        conta = contaService.save(conta);
        return ResponseEntity.ok().body(conta);
    }

    @DeleteMapping("/v1/conta/{id}")
    public <T> ResponseEntity<T> delete(@PathVariable("id") Long id) {
        Conta c = this.contaService.findOne(id);
        this.contaService.delete(c);
        return ResponseEntity.noContent().build();
    }

    @InitBinder("conta")
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(new ContaValidator());
    }

}
