package com.github.deiseabreu.springboot.basicauth.repository;

import com.github.deiseabreu.springboot.basicauth.model.Conta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Conta, Long> {
}
