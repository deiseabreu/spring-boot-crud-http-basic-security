package com.github.deiseabreu.springboot.basicauth.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class Conta {

    public static enum Sexo {
        M, F;
    }

    protected Conta() {}

    public Conta(@NotNull @Size(min = 3, max = 255, message = "Nome deve possuir entre 3 e 255 caracteres") String nome, @NotNull @Size(min = 3, max = 255, message = "Sobrenome deve possuir entre 3 e 255 caracteres") String sobrenome, @NotNull Sexo sexo, @NotNull LocalDate dataNascimento, @NotNull @Pattern(regexp = "[0-9]{9}", message = "Telefone deve possuir apenas dígitos") String telefone, @NotNull @Pattern(regexp =
            "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", message = "e-mail inválido.") String email) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.sexo = sexo;
        this.dataNascimento = dataNascimento;
        this.telefone = telefone;
        this.email = email;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id")
    private Long id;

    @NotNull
    @Size(min = 3, max = 255, message = "Nome deve possuir entre 3 e 255 caracteres")
    @Column(name="nome", nullable = false)
    private String nome;

    @NotNull
    @Size(min = 3, max = 255, message = "Sobrenome deve possuir entre 3 e 255 caracteres")
    @Column(name="sobrenome", nullable = false)
    private String sobrenome;

    @NotNull
    @Column(name="sexo", nullable = false)
    private Sexo sexo;

    @NotNull
    @Column(name="dataNascimento", nullable = false)
    private LocalDate dataNascimento;

    @NotNull
    @Pattern(regexp = "[0-9]{9}", message = "Telefone deve possuir apenas dígitos")
    @Column(name="telefone", nullable = false)
    private String telefone;

    @NotNull
    @Pattern(regexp =
            "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", message = "e-mail inválido.")
    @Column(name="email", nullable = false)
    private String email;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Conta conta = (Conta) o;
        return id.equals(conta.id) &&
                nome.equals(conta.nome) &&
                sexo == conta.sexo &&
                dataNascimento.equals(conta.dataNascimento) &&
                email.equals(conta.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, sexo, dataNascimento, email);
    }
}
