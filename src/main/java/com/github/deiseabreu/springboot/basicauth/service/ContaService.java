package com.github.deiseabreu.springboot.basicauth.service;

import com.github.deiseabreu.springboot.basicauth.model.Conta;
import com.github.deiseabreu.springboot.basicauth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ContaService {

    @Autowired
    private UserRepository repository;

    @Transactional(readOnly = true)
    public Page<Conta> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Conta findOne(Long id) {
        return repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Não foi possível localizar a conta com id %d", id)));
    }

    public Conta save(Conta conta) {
        return repository.save(conta);
    }

    public void delete(Conta conta) {
        repository.delete(conta);
    }

}
