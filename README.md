## Spring Boot CRUD Application

### Endpoints

| Método | URI | Descrição |
| ------ | --- | ---------- |
| GET    |/v2/api-docs     | swagger json |
| GET    |/swagger-ui.html | swagger html |
| GET    |/actuator/info   | info / heartbeat - provided by spring boot actuator |
| GET    |/actuator/health | application health - provided by spring boot actuator |
| GET    |/v1/conta/{id}    | get conta by id |
| GET    |/v1/contas        | get N contas with an offset|
| PUT    |/v1/conta         | add/update conta|
| DELETE |/v1/conta/{id}    | delete conta|

### Author

@deiseabreu<br>
deise.ca@gmail.com<br>
